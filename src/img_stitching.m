function  [] = img_stitching( path , num , focal_length)

%% input image & feature detector--------------------------
disp('read in image to extract feature......');
for i = 1:num
    str(i,:) = sprintf('%s%03d.jpg', path, i);
    MSOP(str(i,:));
end

%% matcher image-----------------------------------
disp('matching the images......');
for i = 1:num-1
    matcher( str(i,:), str(i+1,:));
end

%% image warping-------------------------------------
disp('image warping......');
for i = 1:num
    warping(str(i,:) , focal_length);
end

%% stitching---------------------------------------------
disp('image stitching......');

deltaX = zeros([1 num-1]);
deltaY = zeros([1 num-1]);
for i = 1 : num-1
    [deltaX(i),deltaY(i)] = stitcher( str(i,:),focal_length);
end
fid = fopen('delta.txt','w');
for i = 1 : num-1
    fprintf(fid,'%g %g\n', deltaX(i),deltaY(i));
end
fclose(fid);
% painting---------------------------------------------
disp('painting......');
tmp = sprintf('%s%03d.jpg_warp.bmp', path, 1);
warpimg = double(imread(tmp))/255;
[h,w,~] = size(warpimg);

sumX = sum(deltaX);
sumY = 0;
sumY = [sumY deltaY];
testX = [0 w w+deltaX];

for i = 2 : size(sumY,2)
    sumY(i) = sumY(i) + sumY(i-1);

end
for i = 2 : size(testX,2)
    testX(i) = testX(i) +  testX(i-1);
end

minY = min(sumY);
if minY>0
    minY = 0;
end
maxY = max(sumY);
if maxY < 0
    maxY = 0;
end
outimg = zeros([(h+maxY-minY) (w*num + sumX) 3]);
size(outimg)

threshold = 0;

outimg(maxY+1:h + maxY , 1:w-threshold, :) = warpimg(: , 1:w-threshold ,:) ;
x = w;
% refine X shift
deltaX = deltaX-5;

blend_method = 'mu';
%blend_method = 'alpha';

for k = 2 : num
    tmp = sprintf('%s%03d.jpg_warp.bmp', path, k);
    warpimg2 = double(imread(tmp))/255;
    beld = linspace(1,0,-deltaX(k-1));

    % imshow(warpimg2)
    % line([(-deltaX(k-1) - threshold+1), (-deltaX(k-1) - threshold+1)], [1, 540])
    % hold on;

    if strcmp(blend_method, 'alpha') == 1
        disp("alpha");
        for j = threshold +1: -deltaX(k-1) - threshold

            if(deltaY(k-1) >= 0)
                outimg(maxY-sumY(k-1)+1: h+ maxY-sumY(k) , x+deltaX(k-1)+j,: ) =  outimg(maxY-sumY(k-1)+1: h+ maxY-sumY(k) , x+j+deltaX(k-1) ,: ) * beld(j-threshold) + warpimg2(1+deltaY(k-1):h, j ,:)*(1-beld(j-threshold));
            end
            if(deltaY(k-1) < 0)
                outimg(maxY-sumY(k)+1:h + maxY-sumY(k-1) , x+j+deltaX(k-1) ,: ) =  outimg( maxY-sumY(k)+1:h + maxY-sumY(k-1) , x+j+deltaX(k-1) ,: ) * beld(j-threshold) + warpimg2(1:h+deltaY(k-1), j ,:)*(1-beld(j-threshold));
            end
        end
    else
        disp("multi");
        if(deltaY(k-1) >= 0)
                overlap1 = outimg(maxY-sumY(k-1)+1: h+ maxY-sumY(k) , x+threshold +1+deltaX(k-1):x+-deltaX(k-1) - threshold+deltaX(k-1) ,: );
                overlap2 = warpimg2(1+deltaY(k-1):h, threshold +1: -deltaX(k-1) - threshold ,:);
        elseif(deltaY(k-1) < 0)
                overlap1 = outimg( maxY-sumY(k)+1:h + maxY-sumY(k-1) , x+threshold +1+deltaX(k-1):x+-deltaX(k-1) - threshold+deltaX(k-1) ,: );
                overlap2 = warpimg2(1:h+deltaY(k-1), threshold +1: -deltaX(k-1) - threshold ,:);
        end

        [~, ending, ~] = size(overlap1);
        layer = 1;
        start = 1;
        
        g = cell(2, layer + 1);
        lap = cell(2, layer + 1);
        newG = cell(2, layer + 1);
        newLap = cell(2, layer +1);
        w1 = zeros(1, ending);
        w2 = zeros(1, ending);
        
        g{1, 1} = overlap1;
        g{2, 1} = overlap2;
        g1 = overlap1;
        g2 = overlap2;

        for i = 1: layer
            g1 = imgaussfilt( g1, 10/i );
            g2 = imgaussfilt( g2, 10/i );
            lap{1, i} = double(g{1, i}) - double(g1);
            lap{2, i} = double(g{2, i}) - double(g2);
            g1 = imresize(g1, 0.5);
            g2 = imresize(g2, 0.5);
            g{1, i+1} = g1;
            g{2, i+1} = g2;         
        end
        lap{1, layer + 1} = g{1, layer + 1};
        lap{2, layer + 1} = g{2, layer + 1};
        for i = 1: layer + 1
            sizeG = size(g{1, i}); 
            temp = newLap{1, i};         
            for j = 1: sizeG(2)
                w1(j) = 1 - (j-start) / ((ending-start)/2^(i-1));
                w2(j) = (j-start) / ((ending-start)/2^(i-1));
                temp(:,j,1:3) = w1(j) .* lap{1,i}(:, j, 1:3) + w2(j) .* lap{2,i}(:, j, 1:3);
            end
            newLap{1, i} = temp;
            
        end
        for i = layer: -1: 1
            sizeLap = size(newLap{1, i});
            if i == layer
                temp1 = imresize(newLap{1, i+1}, [sizeLap(1), sizeLap(2)]);
            else
                temp1 = imresize(newG{1, i+1}, [sizeLap(1), sizeLap(2)]);
            end
            newG{1, i} = double(temp1) + double(newLap{1, i});    
        end
        new = newG{1, 1};
        if(deltaY(k-1) >=0)
            outimg(maxY-sumY(k-1)+1: h+ maxY-sumY(k) ,  x+threshold +1+deltaX(k-1):x+-deltaX(k-1) - threshold+deltaX(k-1),: ) = new;
        
        else
           outimg(maxY-sumY(k)+1:h + maxY-sumY(k-1) ,  x+threshold +1+deltaX(k-1):x+-deltaX(k-1) - threshold+deltaX(k-1),: ) = new; 
        end
    end
    % not overlaping
    if(deltaY(k-1) >= 0)
        outimg( maxY-sumY(k)+1: h+ maxY-sumY(k) , x-threshold+1:x + w -threshold+deltaX(k-1)  ,: ) = warpimg2(1:h, -deltaX(k-1) - threshold+1 :w-threshold ,:);
    end
    if(deltaY(k-1) < 0)
        outimg( maxY-sumY(k)+1:h+ maxY-sumY(k) ,x-threshold+1:x + w -threshold+deltaX(k-1) ,: ) = warpimg2(1:h,  -deltaX(k-1) - threshold+1 :w-threshold ,:);
    end
    if(k == num)
        break;
    end
    clear warpimg2;
    x = x + w + deltaX(k-1)-threshold;
end
imwrite(outimg , 'stitch.bmp','bmp');

%drift

sumY
drift_img = zeros([h w*num+sumX 3]);
    for i = 1:(h+maxY-minY)
        for j = 1:(w*num+sumX)
            for k = size(testX,2)-1:-1:1
                if j > testX(k)

                    drift_img(floor(max(sumY(k) * j / (testX(k+1)) + i,1)) ,j ,:) = outimg(i,j,:);
                    break;
                end
            end
        end
    end
    imwrite(drift_img , 'drift.bmp','bmp');


disp('finish!!!');


