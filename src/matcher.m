function [mymin] = matcher( image1, image2)

% read image1
filename = sprintf( '%s.txt',image1);
tmp = dlmread(filename);
point1(:, 1:4) = tmp(:, 1:4);
desc1(:, 5:68) = tmp(:, 5:68);
img1 = imread( image1 );
[H_1, W_1, C_1] = size(img1);

% read image2
filename = sprintf( '%s.txt',image2);
tmp = dlmread(filename);
point2(:, 1:4) = tmp(:, 1:4);
desc2(:, 5:68) = tmp(:, 5:68);
img2 = imread( image2 );
[H_2, W_2, C_2] = size(img2);

threshold = 4;
%{
% output match image
out_img = zeros( [H_1 , (W_1+W_2), 3] );
out_img = uint8(out_img);
out_img = [img1 img2];

for i = 1:size(point1 ,1)
     x = point1(i,1);
     y = point1(i,2);
     level = point1(i,4);
 
     x = round(x*(2^(level-1)));
     y = round(y*(2^(level-1)));
     out_img(y:y+1,x:x+1,1) = 255;
     out_img(y:y+1,x:x+1,2) = 0;
     out_img(y:y+1,x:x+1,3) = 0;
end

for i = 1:size( point2 ,1)
     x = point2(i,1);
     y = point2(i,2);
     level = point2(i,4);
 
     x = round(x*(2^(level-1)))+W_1;
     y = round(y*(2^(level-1)));
     out_img(y:y+1,x:x+1,1) = 255;
     out_img(y:y+1,x:x+1,2) = 0;
     out_img(y:y+1,x:x+1,3) = 0;
end
imshow(out_img);
hold on;
%}
k = 0;
str = sprintf('%s_match.txt' , image1);
fid = fopen(str,'w');
for i = 1:size(point1,1)
    mymin(i) = Inf;
    mymin2   = Inf;
    for j = 1:size(point2,1)
       if( point1( i,4) ~= point2( j,4) )
           continue;
       end
       dist = mydist( desc1(i,1:64) , desc2(j,1:64) );
       if( dist < mymin(i) )
           mymin2    = mymin(i);
           mymin(i)  = dist;
           min_point = j;
       else
           if( dist < mymin2 )
               mymin2 = dist;
           end
       end
    end
    if( (mymin(i) < threshold) && ( 4*mymin(i) < 3*mymin2) )
        k = k +1;
        x1 = point1(i,1);
        y1 = point1(i,2);
        level1 = point1(i,4);
        x1 = round(x1*(2^(level1-1)));
        y1 = round(y1*(2^(level1-1)));

        x2 = point2(min_point,1);
        y2 = point2(min_point,2);
        level2 = point2(min_point,4);
        x2 = round(x2*(2^(level2-1)))+W_1;
        y2 = round(y2*(2^(level2-1)));    
        %line([x1 x2],[y1 y2],'Color','yellow');
        fprintf(fid , '%g\t%g\t%g\t%g\n',x1,y1, x2,y2);
    end       
end

fclose(fid);


         