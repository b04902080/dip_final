function [outimg] = image_subsampling(img)

temp_img = imgaussfilt(img, 1);
outimg = imresize(temp_img, 0.5);