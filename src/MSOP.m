function [outimg] = MSOP( imageName )

outimg = imread(imageName);
img1 = rgb2gray(outimg);
outimg = double(outimg);
img1 = double(img1);
% do multi scale harris corner 
[trimap1] = feature_detector(img1,800);
[img2] = image_subsampling(img1);
[trimap2] = feature_detector(img2,400);
[img3] = image_subsampling(img2);
[trimap3]= feature_detector(img3,200);
% write the features to the file-------------------------------------------
fid = fopen('trimap1.txt','w');
for i = 1 : size(trimap1,1)
    fprintf(fid,'%g %g %g\n', trimap1(i,1) ,trimap1(i,2), trimap1(i,3));
end
fclose(fid);

fid = fopen('trimap2.txt','w');
for i = 1 : size(trimap2,1)
    fprintf(fid,'%g %g %g\n', trimap2(i,1) ,trimap2(i,2), trimap2(i,3)); 
end
fclose(fid);

fid = fopen('trimap3.txt','w');
for i = 1 : size(trimap3,1)
    fprintf(fid,'%g %g %g\n', trimap3(i,1) ,trimap3(i,2), trimap3(i,3));
end
fclose(fid);
% do descriptor-----------------------------------------------------------
disp('MSOP descriptor...');

[temp1, inpMap1, check1] = descriptor( img1 ,'trimap1.txt' );
[temp2, inpMap2, check2] = descriptor( img2 ,'trimap2.txt' );
[temp3, inpMap3, check3] = descriptor( img3 ,'trimap3.txt' );

str = sprintf('%s.txt',imageName);
fid = fopen( str ,'w');
size_inp1 = size(temp1,1);
line = 0;
for k = 1 : size_inp1
    if( check1(k) == 1)
        line = line + 1;
        fprintf( fid,'%g\t%g\t%g\t1 ',temp1(k ,1) ,temp1(k ,2) ,temp1(k ,3));
        fprintf( fid,'%g ', inpMap1(k,1:8,1:8) );
        fprintf( fid,'\n');
    end
end
size_inp2 = size(temp2,1);
line = 0;
for k = 1 : size_inp2
    if( check2(k) == 1)
        line = line + 1;
        fprintf( fid,'%g\t%g\t%g\t2 ',temp2(k ,1) ,temp2(k ,2) ,temp2(k ,3));
        fprintf( fid,'%g ', inpMap2(k,1:8,1:8) );
        fprintf( fid,'\n');
    end
end
size_inp3 = size(temp3,1);
line = 0;
for k = 1 : size_inp3
    if( check3(k) == 1)
        line = line + 1;
        fprintf( fid,'%g\t%g\t%g\t3 ',temp3(k ,1) ,temp3(k ,2) ,temp3(k ,3));
        fprintf( fid,'%g ', inpMap3(k,1:8,1:8) );
        fprintf( fid,'\n');
    end
end
fclose(fid);