function [] = cropping(path,outpath, direction)
% Load images.
imgDir = fullfile(path);
Scene = imageDatastore(imgDir);
numImages = numel(Scene.Files);

for n = 1:numImages
    file_path = strsplit(Scene.Files{n},'\');
    file_index = size(file_path, 2);
    I = readimage(Scene, n);
    if(direction == 1) % right
        I2 = I(:,1:size(I,2)-38,:);
    end
    if(direction == 2) % left
        I2 = I(:,39:size(I,2),:);
    end
    out_path = strcat(outpath,file_path{file_index});
    imwrite(I2,out_path);
end
end