function [ distance ] = mydist( p1, p2 )

% p1 p2 are 8x8 descriptor
tmp = p1 - p2;
tmp = tmp.*tmp;

distance =  sum( tmp ) ;