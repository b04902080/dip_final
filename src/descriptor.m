function   [temp, inpMap ,check] = descriptor( src , filename )
[h,w,~] = size(src);
src = imgaussfilt(src, 1);
temp = dlmread(filename);

size_inp = size(temp,1);
inpMap = zeros([size_inp 8 8]);
check  = ones(size_inp);
for k = 1 : size_inp
    x = temp(k,1);
    y = temp(k,2);
    theta = pi*temp(k,3)/180;
    for i = 1 : 40
        for j = 1 : 40
            offx = j - 20; % x = col from -19 to 20  
            offy = 20 - i; % y = row from 19 to -20
            xx = offx*cos(theta) - offy*sin(theta);
            yy = -(offx*sin(theta) + offy*cos(theta));
            img_x = round(x + xx);
            img_y = round(y + yy);
            if( img_x < 1 || img_x > w || img_y < 1 || img_y > h)
                i = 50;
                j = 50;
                check( k ) = 0;
                break;
            end
            inpMap(k, floor((i-1)/5)+1,floor((j-1)/5)+1 ) = inpMap(k, floor((i-1)/5) + 1,floor((j-1)/5) + 1 ) + src(img_y,img_x)/25; 
        end
    end
end
mu = zeros([size_inp 1]);
sigma = zeros([size_inp 1]);
for i = 1 : size_inp
    if( check(i) == 1)
        mu(i) = sum(sum(inpMap(i, 1:8,1:8))) / 64;
    end
end
for i = 1 : size_inp
    if( check(i) == 1)
        sigma(i) = sqrt( sum( sum( (inpMap(i, 1:8,1:8) - mu(i) ).^2)  ) / 64 );
    end
end
for i = 1 : size_inp
    if( check(i) == 1)
        inpMap(i, 1:8,1:8) = ( inpMap(i, 1:8,1:8) - mu(i) )/sigma(i);
    end
end