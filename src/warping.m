function [] = warping(Input_image , f)
% change image into Cylindrical image

% read image
src = imread(Input_image);
src = double(src);
% width, height, channel
[h, w, c] = size(src);
dist_img = zeros(h,w,c);
min_x = w-1;
max_x = 0;
% f = str2double(f)
for i = 1:h %y
    for j = 1:w %x
        x = f .* atan((j - w./2) ./ f) + w./2;
        y = f * (i-h/2)/(sqrt(f.^2 + (j-w/2).^2)) + h/2;
        
        dist_img(floor(y),floor(x),:) = src(i,j,:);
        if(min_x > x)
            min_x = x;
        end
        if(max_x < x)
            max_x = x;
        end
    end
end
dist_img = dist_img/255;
str = sprintf('%s_warp.bmp', Input_image);
imwrite(dist_img(:, floor(min_x):floor(max_x), :) , str);
