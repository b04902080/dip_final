function [deltaX,deltaY] = stitcher( image1 ,f)
%sprintf('%s_warp.bmp', image1)
tmp = imread(sprintf('%s_warp.bmp', image1));
%h => y, w => x
[h, w, ~] = size(tmp);
center = [w/2 ,h/2];
str = sprintf('%s_match.txt' , image1);
M = dlmread(str);
size_match = size(M, 1);
%f = str2double(f)
disp('ransac...');

for i = 1:size_match
    p(i,1) = f * atan((M(i,1) - center(1))/f) + center(1);
    p(i,3) = f * atan((M(i,3) - center(1))/f) + center(1);
    p(i,2) = M(i,2);
    %p(i,2) = f * ((M(i,2) - center(2)) / sqrt((M(i,1)-center(1))^2 + f^2)) + center(2);
    p(i,4) = M(i,4);
    %p(i,4) = f * ((M(i,4) - center(2))/ sqrt((M(i,3)-center(1))^2 + f^2)) + center(2);
end

%p = M;
deltaX = 0;
deltaY = 0;
r = zeros([1 size_match]);
threshold = 30;
for i = 1 : size_match
   deltaX = p(i,3) - p(i,1);
   deltaY = p(i,4) - p(i,2); 
   
   for j = 1 : size_match
       deltaX2 = p(j,3) - p(j,1);
       deltaY2 = p(j,4) - p(j,2); 
       
       dic = (deltaX2-deltaX)^2 +  (deltaY2-deltaY) ^ 2;
       if(dic < threshold)
           r(i) = r(i) +1;
       end
   end   
end
[value, index] = max(r);

deltaX2 = p(index,3) - p(index,1);
deltaY2 = p(index,4) - p(index,2);
deltaX3 = 0;
deltaY3 = 0;
for i = 1:size_match
   deltaX = p(i,3) - p(i,1);
   deltaY = p(i,4) - p(i,2);
   dic = (deltaX2-deltaX)^2 +  (deltaY2-deltaY)^2;
   if(dic < threshold)
          deltaX3 = deltaX3 + deltaX;
          deltaY3 = deltaY3 + deltaY;
   end
end

deltaX = -round(deltaX3/value);
deltaY = round(deltaY3/value);
