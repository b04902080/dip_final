% number specify number of feature you want
function [refine_inp] = feature_detector(img, number)
%% apply 
[h, w, ~] = size(img);
img = double(img);
g_img = imgaussfilt(img, 1);
dfx = zeros([h w]);
dfy = zeros([h w]);
%% expand image
temp = zeros([h+2 w+2]);
temp( 2:h+1 , 2:w+1 ) = g_img(1:h,1:w);
temp( 1, 2:w+1 ) = g_img(1,1:w);
temp( h+2, 2:w+1 ) = g_img(h,1:w);
temp( 2:h+1 , 1) = g_img(1:h,1);
temp( 2:h+1 , w+2) = g_img(1:h,w);

%% compute dfx dfy
for i = 3 : 1 : w+2
    dfx(:,i-2) = (temp(2:h+1,i) - temp(2:h+1,i-2))*0.5;
end

for i = 3 : 1 : h+2
    dfy(i-2 ,:) = -0.5*(temp(i,2:w+1) - temp(i-2,2:w+1));
end
%%calculate fHM
dXX = imgaussfilt(dfx.*dfx, 1.5);
dYY = imgaussfilt(dfy.*dfy, 1.5);
dXY = imgaussfilt(dfx.*dfy, 1.5);
Det = dXX.*dYY - dXY.*dXY;
tr = dXX+dYY;
Fhm = zeros([h w]);
indexno0 = find(tr ~= 0);
Fhm(indexno0) = Det(indexno0) ./ tr(indexno0);
%% Sub-pixel accuracy------------------------------------------------------
temp = zeros([h+2 w+2]);
temp( 2:h+1 , 2:w+1 ) = Fhm(1:h,1:w);
temp( 1, 2:w+1 ) = Fhm(1,1:w);
temp( h+2, 2:w+1 ) = Fhm(h,1:w);
temp( 2:h+1 , 1) = Fhm(1:h,1);
temp( 2:h+1 , w+2) = Fhm(1:h,w);
dfdx = zeros([h w]);
dfdy = zeros([h w]);
ddfdxx = zeros([h w]);
ddfdyy = zeros([h w]);
ddfdxdy = zeros([h w]);
for i = 3 : 1 : w+2
    dfdx(:,i-2) = (temp(2:h+1, i) - temp(2:h+1, i-2))*0.5;
    ddfdxx(:,i-2) = temp(2:h+1,i) + temp(2:h+1,i-2) - 2*temp(2:h+1,i-1);
    ddfdxdy(:,i-2) =  ( ( temp(1:h,i) + temp(3:h+2,i-2) ) - ( temp(3:h+2,i) + temp(1:h,i-2) ) )/4;
end

for i = 3 : 1 : h+2
    dfdy(i-2,:) = -0.5*(temp(i,2:w+1) - temp(i-2,2:w+1));
    ddfdyy(i-2,:) = temp(i,2:w+1) + temp(i-2,2:w+1) - 2*temp(i-1,2:w+1);
end

%% find the local maxima
k = 0;
F_points = [];
for i = 2:h-1 % i for row
    for j = 2:w-1 % j for col
        % 3x3 local window
        if (Fhm(i,j) == max(max(Fhm(i-1:i+1 , j-1:j+1))))  &&  Fhm(i,j)>10
            A = [];
            B = [];
            k = k+1;
            Fhm_value = Fhm(i,j);
            % sub pixel refinement and notice the coordinate of matlab
            A = [ddfdxx(i,j) ,ddfdxdy(i,j) ; ddfdxdy(i,j), ddfdyy(i,j)] ;
            B = [dfdx(i,j) ; dfdy(i,j)];
            B = -B;
            X = A \ B;
            
            tmpX = round(j + X(1)); % col
            tmpY = round(i - X(2)); % row
            % if out of bound, drop this point
            if (tmpX < 2 || tmpX > w-1 || tmpY < 2 || tmpY > h-1)
                k = k-1;
                continue;
            end

            F_points(k,1) = j + X(1); % record col of point / X
            F_points(k,2) = i - X(2); % record row of point / Y
            
            %% find orientation
            tmp_row = round(F_points(k,2));
            tmp_col = round(F_points(k,1));
            Blur_img = imgaussfilt(img(tmp_row-1:tmp_row+1 , tmp_col-1:tmp_col+1),4.5);
            B_dx = (Blur_img(2,3) - Blur_img(2,1))*0.5;
            B_dy = (Blur_img(1,2) - Blur_img(3,2))*0.5;
            B_dist = sqrt(B_dx*B_dx+B_dy*B_dy);

            theta = (acos(B_dx/B_dist)/pi)*180;
            if( (B_dy/B_dist) < 0 )
                theta = 360 - theta;
            end
            F_points(k,3) = theta;
            F_points(k,4) = Fhm_value;
        end
    end
end

% ANMS
Crobust = 0.9;
size_inp = size( F_points,1);
r = [];
for i = 1:size_inp
    min_r = Inf;
    for j = 1:size_inp
        if j == i
            continue;
        end
        if( F_points(i,4) < (F_points(j,4) * Crobust) )
            tmp_r = (F_points(i,1) - F_points(j,1))*(F_points(i,1) - F_points(j,1)) + (F_points(i,2) - F_points(j,2))*(F_points(i,2) - F_points(j,2));
            if( tmp_r < min_r )
                min_r = tmp_r;
            end
        end
    end
    r( i ) = min_r;
end
% refine_inp = [col row theta]
refine_inp = [];
[~, index] = sort(r);
size_i = size( index,2);
number = min( [ number, size_i] );
for i = 1:number
    refine_inp(i,1:3) = F_points( index(1,size_i - i + 1),1:3);
end
%{
for i = 1:size(refine_inp,1)
    img(floor(refine_inp(i,2)), floor(refine_inp(i,1)),1) = 255;
    img(floor(refine_inp(i,2)), floor(refine_inp(i,1)),2) = 0;
    img(floor(refine_inp(i,2)), floor(refine_inp(i,1)),3) = 0;
end

img = img ./255;
imshow(img);
%}
end